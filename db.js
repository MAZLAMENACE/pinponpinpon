const mongoose = require("mongoose");

const { database, databaseName } = require('./env');

const connect = async () => {
  try {
    return await mongoose.connect(`${database}${databaseName}`);
  } catch (err) {
    console.log('Error while connecting to mongoose.', err);
  }
};

const disconnect = async () => {
  await mongoose.disconnect();
};

const dbLogs = () => {
  mongoose.connection.on("Connected", () => console.log(`Connected with mongoose on ${database}`));
  mongoose.connection.on("open", () => console.log(`Opening ${databaseName}`));
  mongoose.connection.on("disconnected", () => console.log(`Disconnected with mongoose on ${database}`));
  mongoose.connection.on("reconnected", () => console.log(`Reconnected with mongoose on ${database}`));
  mongoose.connection.on("disconnecting", () => console.log(`Disconnecting with mongoose on ${database}...`));
  mongoose.connection.on("close", () => console.log(`Connection with mongoose on ${database} closed.`));
};

module.exports = {
  connect,
  disconnect,
  dbLogs,
}