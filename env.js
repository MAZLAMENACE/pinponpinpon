const inputDataPath = 'data/StockEtablissement_utf8.csv'; // Chemin vers le CSV des établissements
const outputDataSplitted = 'splitted'; // Fichiers de destination des CSV splités
const prefix = 'split'; // préfixe des fichiers, ex : split-0, split-1...
const database = 'mongodb://127.0.0.1:27017/';
const databaseName = 'pinpon'

module.exports = {
  inputDataPath,
  outputDataSplitted,
  prefix,
  database,
  databaseName
}