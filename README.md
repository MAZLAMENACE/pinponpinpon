# Pinponpinpon

## Description

Pinponpinpon est une application JavaScript d'indexation des données CSV du gouvernement sur la base [Sirene](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/) des entreprises et de leurs établissements dans une base noSql MongoDB.

## Architecture

Le projet comporte plusieurs parties ayant chacunes leur responsabiltié. Etant donné la haute volumétrie du fichier StockEtablissement, plusieurs processus ont été mis en place pour permettre l'indexation.

### env.js

Contient toutes les variables d'environnement du projet. Peut être modifié selon les besoins.

### splitter.js

Découpe le fichier StockEtablissement en plusieurs fichiers de taille plus raisonnable. Le fichier d'entrée est spécifié dans `env.js`, ainsi que d'autres paramètres liés à la découpe.

### db.js

Permet de se connecter à la base de données et d'effectuer les opérations CRUD. Les informations de connexion sont spécifiées dans `env.js`.

### csvParser.js

Effectue les traitements ligne par ligne des fichiers CSV pour les transformer en schémas Mangoose. Il est également responsable de l'insertion en base de données MongoDB.

### model.js

Représentation des établissements Sirène par un schéma Mangoose.

### utils.js

Contient la fonction pour convertir / nettoyer la donnée.

### worker.js

Représente la logique d'un worker au sens `pm2` afin de paralléliser les insertions des fichiers divisés. Il utilise le parseur CSV et insère les données en base de données. Chaque traitement est remonté au processus princpal.

### index.js

Fichier principal qui permet de coordoner tous les autres fichiers. Il commence par se connecter à la base de données, crée plusieurs instances de `worker.js` pour indexer les fichiers en parallèle, tout en mesurant le temps d'indexation.

# Installation

1. Node.js et MongoDb doivent être installés sur votre système
2. Clonez le projet Pinponpinpon sur votre machine.
3. Exécutez `npm install` pour installer les dépendances.
4. Exécutez `node splitter.js` pour diviser le fichier CSV.
5. Exécutez `node index.js` pour démarrer le processus d'indexation et suivre les traitements. Vous pouvez voir plus en détails les workers avec la commande `pm2 monit`.

# Configuration

Toute la configuration est centralisée dans le fichier `env.js`. Vous pouvez éditer les paramètres pour convenir à vos besoins.

# Notes

Pour avoir plus de détails sur le fonctionnement du programme, vous pouvez vous référer au dossier `schemas` contenant le schéma d'activité et le schéma d'indexation.
