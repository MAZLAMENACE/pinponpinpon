const csvSplitStream = require('csv-split-stream');
const fs = require('fs');
const path = require('path');
const { inputDataPath, outputDataSplitted, prefix } = require('./env');

const inputCSVPath = path.join(__dirname, inputDataPath);
const outputFolder = path.join(__dirname, outputDataSplitted);

// Créez le dossier de sortie s'il n'existe pas déjà
if (!fs.existsSync(outputFolder)) {
  fs.mkdirSync(outputFolder);
}

csvSplitStream.split(
  fs.createReadStream(inputCSVPath),
  {
    lineLimit: 100_000
  },
  (index) => fs.createWriteStream(path.join(outputFolder, `${prefix}-${index}.csv`)) // Utilisation du chemin relatif pour les fichiers de sortie dans le dossier de sortie
)
  .then(csvSplitResponse => {
    console.log('csvSplitStream succeeded.', csvSplitResponse);
  })
  .catch(csvSplitError => {
    console.log('csvSplitStream failed!', csvSplitError);
  });
