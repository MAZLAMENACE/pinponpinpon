const fs = require("fs");
const path = require('path');
const pm2 = require('pm2');
const { connect, disconnect, dbLogs } = require("./db");
const { outputDataSplitted, prefix } = require('./env');

const files = fs.readdirSync(outputDataSplitted).filter(file => path.extname(file) === ".csv");

dbLogs();
start();

async function start() {
  await connect();
  const indexMax = files.length;
  let i = 0;
  let processDeleted = 0;
  const startTime = process.hrtime(); // Enregistrer le temps de début du processus

  pm2.start({
    script: 'worker.js',
    name: `worker`,
    exec_mode: 'cluster',
    instances: 'max',
  }, (error) => {
    if (error) {
      console.error(error);
      pm2.disconnect();
      disconnect();
      process.exit(1);
    }
  });

  pm2.connect((err) => {
    if (err) {
      console.error(err);
      process.exit(2);
    }

    pm2.launchBus((workerError, bus) => {
      bus.on('process:msg', (packet) => {
        const workerId = packet.process.pm_id;
        console.log("Message received from Worker - ", workerId);
        const data = packet.data;
        if (data.state === 'idle') {
          console.log("Working ...");
          if (data.state === 'idle' && data.file) {
            console.log(`File processed: ${data.file} - Duration: ${data.duration} seconds . Total files : ` + indexMax);
          }
          if (i < indexMax) {
            pm2.sendDataToProcessId(workerId, {
              id: workerId,
              type: 'process:msg',
              data: {
                file: `${outputDataSplitted}/${prefix}-${i++}.csv`
              },
              topic: true
            }, (messageError) => {
              if (messageError) {
                console.error(`Error when messaging ${workerId}`, messageError);
              }
            });
          } else {
            console.log("Deleting Worker - ", workerId);
            pm2.delete(workerId, (err, proc) => {
              if (err) console.log('erreur', err)
              if (!err) {
                processDeleted++; // Incrémente le compteur lors de la suppression d'un processus
                if (processDeleted === instances) {
                  console.log("All workers have been deleted.");
                  const endTime = process.hrtime(startTime); // Enregistrer le temps de fin du processus
                  const totalTimeSeconds = endTime[0] + endTime[1] / 1e9; // Convertir en secondes
                  const totalTimeMinutes = Math.floor(totalTimeSeconds / 60); // Convertir en minutes
                  const remainingSeconds = Math.round(totalTimeSeconds % 60); // Calculer les secondes restantes
                  console.log(indexMax + " Files have been processed.")
                  console.log(`Total processing time: ${totalTimeMinutes} min ${remainingSeconds} s`);
                  console.log("Job is done.");
                  pm2.disconnect();
                  disconnect();
                  console.log("Exit terminal by CTRL+C.");
                }
              }
            });
          }
        }
      });
    });
  });
}
