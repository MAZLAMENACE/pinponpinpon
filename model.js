const mongoose = require('mongoose');

const sirene = new mongoose.Schema({
  siren: String,
  nic: String,
  siret: String,
  dateCreationEtablissement: Date,
  dateDernierTraitementEtablissement: Date,
  typeVoieEtablissement: String,
  libelleVoieEtablissement: String,
  codePostalEtablissement: String,
  libelleCommuneEtablissement: String,
  codeCommuneEtablissement: String,
  dateDebut: Date,
  etatAdministratifEtablissement: String
}, { versionKey: false });

const SireneSchema = mongoose.model('Etablissements', sirene);

module.exports = SireneSchema;
