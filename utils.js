

/**
 * Nettoie la donnée 
 * - Gestion des null
 * - Converti en Date
 * @param {*} value 
 * @param {*} type 
 * @returns 
 */
function cleanData(value, type = "string") {
  if (value === "" || value === undefined || value === null) {
    return null;
  }
  try {
    if (type === "date") {
      const date = new Date(value);
      return isNaN(date.getTime()) ? null : date;
    }
  } catch (error) {
    return null;
  }
  return value;
}

module.exports = {
  cleanData
};